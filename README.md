# pintinhobash


**Uma imagem docker em bash para execução lenta de APIs** 🐌

**Descrição**
Esta imagem contém 2 APIs em bash simples que executam operações lentamente de forma concorrente e assíncrona, balanceadas por um proxy Nginx que faz auto escalonamento horizontal e vertical.

As APIs gravam temporariamente dados em memória em vez de disco para melhor performance. É usado o Postgres também em memória para armazenar dados de forma distribuída entre instâncias.

Há tratamento inteligente de erros e logs para debug remoto. As APIs também rodam em VMs redundantes em diferentes regiões GCP para alta disponibilidade.

**Tecnologias usadas**
Bash
Nginx para proxy e load balancing
Postgres em memória
Docker + Docker compose
GCP VMs

**Estrutura de arquivos**

    .
    ├── api1.sh
    ├── api2.sh 
    ├── db.sql
    ├── docker-compose.yml
    ├── nginx.conf
    ├── run.sh
    └── README.md

**Como rodar**
./run.sh

**Auto escalonamento**
O Nginx escala horizontalmente instanciando novas APIS quando a carga sobe. Escala também verticalmente aumentando recursos das APIs quando fica acima de 90% de uso.

**Referências inspiradoras**
leandro • nsp
putaria do powergameBR
porradas do akita
helloworldenferrujado
Nginx - O proxy preferido do Lunix Torvalds

**Observações finais**
Essa imagem foi feita como um desafio em bash para a Rinha de Backend. Na vida real é melhor usar linguagens + eficientes e menos lentas rs

Espero que gostem desse bashjob lento e descontraído. se não gostar tb, foda-se, desde que fique entre os últimos 10 tah bohn 😜👍

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
